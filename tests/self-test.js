const {suite, skip, assert, Status} = require('./micro-test.js');

var res = suite('Self-check')
    .test('Check assertion', () => {
        try {
            assert(1 === 2, '1 !== 2');
            throw new Error('not expected');
        } catch (e) {
            if (e.message === 'not expected')
                throw e;
        }
    })
    .test('Check skipping', () => skip('skipped'))
    .test('Check assertion (positive)', () => assert(1 === 1, '1 === 1'))
    .run();

    console.log(res);