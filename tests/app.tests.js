const {suite, skip, assert} = require('./micro-test.js');
const app = require('../src/index.js');

const res = 
    suite('App tests')
    .test('Decode basic', () => {
        const {tick, chords} = app.decode('t:45 m:16 7 14 23 21 23 14 23 14');
        assert(tick.toFixed(0) === '333', 'tick length mismatch: ' + tick.toFixed(3));
        const notes = [7, 14, 23, 21, 23, 14, 23, 14];
        chords.flat(1).map((x, i) => assert(x === notes[i], `decoding mismatch at ${i}`));
    })
    .test('Decoding pauses', () => {
        const {chords} = app.decode('t:1 m:1 1 p 2 p p 3 p p p 4');
        assert(chords.length === 10, 'some parts missed');
        assert((chords[1].length + chords[3].length + chords[4].length + chords[6].length + chords[7].length + chords[8].length) === 0,
            'all specified indices should be empty');
        assert(chords[0][0] === 1, 'first note');
        assert(chords[2][0] === 2, 'second note');
        assert(chords[5][0] === 3, 'third note');
        assert(chords[9][0] === 4, 'fourth note');
    })
    .run();

console.log(res);