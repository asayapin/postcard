class Status {
    constructor(passed, name, duration, message){
        this.passed = passed;
        this.message = message;
        this.name = name;
        this.duration = duration;
    }

    toString(){
        const symbol = this.passed === true ? '✓' : this.passed === false ? '⨉' : '⇌';
        const firstPart = `${symbol} [${(this.duration / 1000).toFixed(3)} s] ${this.name}`;
        const secondPart = this.message.length > 0 ? `\n  ${this.message}` : '';
        return firstPart + secondPart;
    }
}

class Test{
    constructor(name, callback){
        this.name = name;
        this.callback = callback;
    }

    run(){
        const start = new Date();
        try{
            this.callback();
            const end = new Date();
            return new Status(true, this.name, end - start, '');
        }
        catch(e){
            const end = new Date();
            if (e instanceof Error && e.message.indexOf('Inconclusive: ') === 0){
                return new Status(undefined, this.name, end - start, e.message.substring(14));
            }
            return new Status(false, this.name, end - start, e.toString());
        }
    }
}

class Suite{
    constructor(name){
        this.name = name;
        this.tests = [];
    }

    test(name, callback){
        this.tests.push(new Test(name, callback));
        return this;
    }

    run() {
        return `== ${this.name} (${this.tests.length} tests) ==\n` + this.tests.map(x => x.run().toString()).join('\n');
    }
}

module.exports = {
    suite: (name) => new Suite(name),
    skip: (message) => { throw new Error('Inconclusive: ' + message); },
    assert: (cond, reason) => { if (!cond) throw new Error(reason); }
};