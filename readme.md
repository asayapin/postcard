# Postcard

[![pipeline status](https://gitlab.com/asayapin/postcard/badges/master/pipeline.svg)](https://gitlab.com/asayapin/postcard/-/commits/master)

The postcard is deployed to [the pages](https://asayapin.gitlab.io/postcard)

A very simple interactive beeper music box

## Copyrights

[Frame](https://wallup.net/nature-forest-window-fall-snow-winter/) | 
[Forest](https://wallpapersafari.com/w/0RM2cY) | 
[Showflake](https://www.clipartmax.com/middle/m2i8d3m2K9N4b1H7_snowflake-icon/) | 
[Musicbox](https://www.etsy.com/listing/597870122/inlay-music-box-wood-inlay-music-box)

Not that I care too much - IMO, all art objects should be free (and ones that are paid are too often have nothing to do with art)