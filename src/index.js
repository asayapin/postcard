// 0 1  2
// C C# D
// 3  4 5 
// D# E F 
// 6  7 8
// F# G G# 
// 9 10 11
// A A# B

const BachCelloSuiteGMaj = 't:60 m:16' +
    ' 7 14 23 21 23 14 23 14 7 14 23 21 23 14 23 14' +
    ' 7 16 24 23 24 16 24 16 7 16 24 23 24 16 24 16' +
    ' 7 18 24 23 24 18 24 18 7 18 24 23 24 18 24 18' +
    ' 7 19 23 21 23 19 23 19 7 19 23 21 23 19 23 19' +
    ' 7 16 23 21 23 19 18 19 16 19 18 19 11 14 13 11' +
    ' 13 19 21 19 21 19 21 19 13 19 21 19 21 19 21 19' +
    ' 18 21 26 25 26 21 19 21 18 21 19 21 14 18 16 14' +
    ' 4 11 19 18 19 11 19 11 4 11 19 18 19 11 19 11' +
    ' 4 13 14 16 14 13 11 9 19 18 16 26 25 23 21 19' +
    ' 18 16 14 26 21 26 18 21 14 16 18 21 19 18 16 14' +
    ' 26 p p p';

const RiverFlowsInYou = 't:70 m:32 ' +
    '21 p p p 20 p 21 p p p p p 20 p 21 p p p p p 15 p 21 p p p p p 13 p p p ' +
    '12 p p p 13 p p p 15 p p p 12 p p p 10 p p p p p p p p p p p 8 p 7 p ' +
    '8 p p p p p p p p p p p 8 p 10 p 12 p p p p p p p p p p p 12 p 13 p ' +
    '15 p p p p p p p p p p p 13 p 12 p 10 p p p p p p p p p p p p p p p ' +
    '21 p p p 20 p 21 p p p p p 20 p 21 p p p p p 15 p 21 p p p p p 13 p p p ' +
    '12 p p p 13 p p p 15 p p p 25 p p p 23 p p p p p p p 25 p 23 p 21 p 20 p ' +
    '21 p p p p p p p p p p p 8 p 10 p 12 p p p p p p p p p p p 12 p 13 p ' +
    '15 p p p p p p p p p p p 13 p 12 p 10 p p p p p p p 21 p 23 p 21 p 20 p ' +
    '21 p p p 15 p p p 21 p 23 p 21 p 20 p 21 p p p 15 p p p 21 p 23 p 21 p 20 p ' +
    '21 p 23 p 25 p 26 p 28 p 25 p 23 p 21 p 20 p p p 15 p p p 21 p 23 p 21 p 20 p ' +
    '21 p p p 15 p p p 21 p 23 p 21 p 20 p 21 p p p 15 p p p 21 p 23 p 21 p 20 p ' +
    '21 p 23 p 25 p 26 p 28 p 25 p 23 p 21 p 20 p p p 15 p p p 21 p 23 p 21 p 20 p ' +
    '21 p p p 15 p p p 21 p 23 p 21 p 20 p 21 p p p 15 p p p 21 p 23 p 21 p 20 p ' +
    '21 p 23 p 25 p 26 p 28 p 25 p 23 p 21 p 20 p p p 15 p p p 21 p 23 p 21 p 20 p ' +
    '21 p p p 15 p p p 21 p 21 23 21 p 20 p 21 p p p 15 p p p 21 p 21 23 21 p 20 p ' +
    '21 p 23 p 25 p 26 p 28 p 25 p 23 p 21 p 20 p p p 15 p p p 7 p p p 3 p p p ' +
    '21 p p p 20 p p p 21 p p p 20 p p p 21 p p p 15 p p p 21 p p p 13 p p p ' +
    '12 p p p 13 p p p 15 p p p 12 p p p 10 p p p p p p p p p p p 8 p 7 p ' +
    '8 p p p p p p p p p p p 8 p 10 p 12 p p p p p p p p p p p 12 p 13 p ' +
    '15 p p p p p p p p p p p 13 p 12 p 10 p p p p p p p p p p p p p p p ' +
    '21 p p p 20 p p p 21 p p p 20 p p p 21 p p p 15 p p p 21 p p p 13 p p p ' +
    '12 p p p 13 p p p 15 p p p 25 p p p 23 p p p p p p p p p p p 21 p 20 p ' + 
    '21 p p p p p p p p p p p 21 p 23 p 25 p p p p p p p p p p p 25 p 26 p ' +
    '28 p p p p p p p p p p p 26 p 25 p 23 p p p p p p p p p p p p p p p ';

const DayByDay = 't:120 m:8 ' + 
    '7 p 19 p 5 17 p 16 p 14 16 19 21 19 17 19 ' + 
    '7 p 19 p 5 17 p 16 p 14 16 19 21 19 17 19 ' + 
    '7 p 19 p 5 17 p 16 p 14 16 17 19 21 23 19 ' + 
    '7 p 19 p 5 17 p 16 p 14 17 16 22 21 17 19 ' + 
    '7 p 19 p 5 17 p 16 p 14 16 19 21 19 17 19 ' + 
    '7 p 19 p 5 17 p 16 p 14 17 16 22 21 17 19 ' + 
    '7 p 19 p 5 17 p 16 p 14 16 17 19 21 23 19 ' + 
    '7 p 19 p 5 17 p 16 p p p p 21 17 12 p';

const knownScores = {
    bach: BachCelloSuiteGMaj,
    river: RiverFlowsInYou,
    fk: DayByDay
};

var globalState = {
    countToPlay: 0,
    current: 0,
    rotations: 0,
    rotorId: undefined,
    isPlaying: false,
    score: undefined,
    playbackDelay: undefined
};

function logState(evt) {
    console.debug(`${evt}; ${globalState.rotations} | ${globalState.countToPlay} | ${globalState.playbackDelay}`);
}

/**
 * input format is
 *  t:120 m:16 0 0+1 0+1+2+3+4+5+6 p p p p
 * where 
 *  t:number - tempo (bpm)
 *  m:number - minimal duration (powers of two in general)
 *  number - note index
 *  + - poliphony
 *  p - pause
 * each entry (space-separated) is of minimal duration
 */

/**
 * 
 * @param {string} s 
 */
function decode(s) {
    const arr = s.split(' ');
    if (arr.length < 2) return {};

    const tempo = arr.filter(x => x.startsWith('t:')).map(x => parseInt(x.substring(2)))[0];
    const minNote = arr.filter(x => x.startsWith('m:')).map(x => parseInt(x.substring(2)))[0];
    const tick = (60 / tempo) * (4 / minNote) * 1000;

    const chords = arr.filter(x => /^(p|\d[\d\+]*)$/.test(x)).map(x => x === 'p' ? [] : x.split('+').map(y => parseInt(y)));

    return { tick: tick, chords: chords };
}

const midNote = Math.pow(2, 1 / 12);
const cBase = 110 * Math.pow(midNote, 3);
function getFreq(num) {
    if (num === 0) return cBase;
    if (num > 0) return cBase * Math.pow(midNote, num);
    return 0;
}

function audioContext() { return new (window.AudioContext || window.webkitAudioContext || window.audioContext); }

/**
 * 
 * @param {number} duration milliseconds
 * @param {number} frequency Hz
 * @param {number} volume 0-1 floating point
 * @param {string} type wave type (use sine)
 * @param {() => void} callback to call on end of playback
 */
function beep(duration, frequency, volume, type, callback) {
    const audioCtx = window.audioCtx;
    var oscillator = audioCtx.createOscillator();
    var gainNode = audioCtx.createGain();

    oscillator.connect(gainNode);
    gainNode.connect(audioCtx.destination);

    if (volume) { gainNode.gain.value = volume; }
    if (frequency) { oscillator.frequency.value = frequency; }
    if (type) { oscillator.type = type; }
    if (callback) { oscillator.onended = callback; }

    oscillator.start(audioCtx.currentTime);
    oscillator.stop(audioCtx.currentTime + ((duration || 500) / 1000));
};

/**
 * 
 * @param {{tick:number, chords:number[][]}} decoded 
 */
function toScore(decoded) {
    return {
        tick: decoded.tick,
        chords: decoded.chords.map(x => x.map(getFreq))
    };
}

/**
 * 
 * @param {{tick:number, chords:number[][]}} score 
 * @param {(current:number, played: number) => void} callback
 */
function play(score, count, startAt, callback) {
    if (globalState.isPlaying || !score) return;
    globalState.isPlaying = true;
    fadeBox();
    let chords = [...score.chords];
    let leftCount = count;
    logState('start');

    for (let i = 0; i < leftCount; i++) {
        const offset = i * score.tick;
        const chord = chords[(startAt + i) % chords.length];
        if (chord.length === 0) continue;
        if (i + 1 < leftCount)
            setTimeout((c) => c.forEach(f => beep(score.tick + 2, f, 0.2)), offset, chord);
        else
            setTimeout((c) => {
                c.forEach(f => beep(score.tick + 2, f, 0.2));
                if (callback) callback((startAt + count) % chords.length, count);
                globalState.isPlaying = false;
                logState('played');
            }, offset, chord);
    }
}

function rotateKey() {
    const keyClasses = document.querySelector('.key').classList;
    keyClasses.toggle('rot180');
    keyClasses.toggle('rot360');
}

function createRotationHandler() {
    globalState.rotorId = setInterval(() => {
        if (globalState.rotations === 0) {
            clearInterval(globalState.rotorId);
            globalState.rotorId = undefined;
        } else {
            rotateKey();
            globalState.rotations--;
            logState('+rot');
        }
    }, 300);
}

function fadeClass(c) {
    let classes = document.querySelector(c).classList;
    classes.remove('show');
    classes.add('fade');
}

function showClass(c) {
    let classes = document.querySelector(c).classList;
    classes.remove('fade');
    classes.add('show');
}

function fadeBox() {
    fadeClass('.mbxc');
    showClass('.bg');
}

function restoreBox() {
    fadeClass('.bg');
    showClass('.mbxc');
}

function handleKeyClick() {
    globalState.rotations++;
    globalState.countToPlay += 30;
    logState('click');
    if (globalState.rotorId === undefined)
        createRotationHandler();

    if (globalState.playbackDelay !== undefined)
        clearTimeout(globalState.playbackDelay);

    globalState.playbackDelay = setTimeout(() => {
        logState('req start');
        play(globalState.score, globalState.countToPlay, globalState.current, (c, p) => {
            globalState.current = c;
            globalState.countToPlay -= p;
            restoreBox();
        });
    }, 2000);
}

function rnd(upTo, fixedTo) {
    return (Math.random() * upTo).toFixed(fixedTo || 1);
}

function prepareSnow() {
    const target = document.querySelector('#snow');
    const flakesCount = 200;

    for (let i = 0; i < flakesCount; i++) {
        const img = document.createElement('img');
        img.src = 'assets/flake.png';
        img.style.left = `${rnd(100, 2)}%`;
        img.style.animationDelay = `-${rnd(15)}s`;
        img.style.animationDuration = `15s`;
        img.style.animationIterationCount = 'infinite';
        img.style.animationName = 'fall';
        img.style.animationTimingFunction = 'linear';

        target.appendChild(img);
    }
}

function onLoad() {
    prepareSnow();
    globalState.score = toScore(decode(BachCelloSuiteGMaj));
    const parts = window.location.href.split('?');
    if (parts.length > 1) {
        const playArg = parts[1].split('&').map(x => x.split('=')).filter(x => x[0].toLowerCase() === 'play');
        if (playArg.length > 0) {
            const arg = knownScores[playArg[0][1]] || Buffer.from(playArg[0][1], 'base64').toString();
            globalState.score = toScore(decode(arg));
        }
    }
}

try {
    if (typeof window === 'undefined')
        module.exports = {
            play,
            toScore,
            decode,
            beep,
            getFreq,
            BachCelloSuiteGMaj
        };
    else {
        window.audioCtx = audioContext();
        document.addEventListener('DOMContentLoaded', onLoad);
    }
} catch (err) {
    if (err.message !== 'ReferenceError: module is not defined')
        console.error(err);
}